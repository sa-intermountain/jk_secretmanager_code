package com.SecretsManager;
import java.util.Base64;
import com.amazonaws.services.secretsmanager.*;
import com.amazonaws.services.secretsmanager.model.*;

public class LambdaRDS{
    public static String[] getSecret() {
	    String region = "us-west-2";
	    AWSSecretsManager client = AWSSecretsManagerClientBuilder.standard().withRegion(region).build();
	    String decodedBinarySecret = "Didnt";
	    String secretName = "secret-master";
		GetSecretValueRequest getSecretValueRequest = new GetSecretValueRequest().withSecretId(secretName);
	    GetSecretValueResult getSecretValueResult = null;
        String [] credentials = new String[3];
	    try{
	        getSecretValueResult= client.getSecretValue(getSecretValueRequest);
		
		} catch (DecryptionFailureException e) {
	    	System.out.print(e.getCause());
	    }catch (InternalServiceErrorException e) {
	        // An error occurred on the server side.
	    		System.out.print(e.getCause());
	        throw e;
	    } catch (InvalidParameterException e) {
	        // You provided an invalid value for a parameter.
	    		System.out.print(e.getCause());
	        throw e;
	    } catch (InvalidRequestException e) {
	        // You provided a parameter value that is not valid for the current state of the resource.
	    		System.out.print(e.getCause());
	        throw e;
	    } catch (ResourceNotFoundException e) {
	        // We can't find the resource that you asked for.
	    		System.out.print(e.getCause());
	        throw e;
	    }
	    
	    if (getSecretValueResult.getSecretString() != null) {
	        String secret = getSecretValueResult.getSecretString();
	        String username = getSecretValueResult.getName();
	        String[] list = secret.split("\"");
	        credentials[0] = username;
	        credentials[1]=list[7]; //password
	        credentials[2]=list[15];//hostname
		    return credentials;
	    } else {
	        decodedBinarySecret = new String(Base64.getDecoder().decode(getSecretValueResult.getSecretBinary()).array());
	        System.out.println(decodedBinarySecret);
		    return credentials;
	    }
	}
}
