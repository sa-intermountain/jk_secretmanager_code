package com.SecretsManager;

import com.SecretsManager.LambdaRDS;

public class Main {
    public static void main(String[] args) {
        String[] credentials = LambdaRDS.getSecret();
    	System.out.println(credentials[0]); //username
        System.out.println(credentials[1]); //password
        System.out.println(credentials[2]); //hostname
    }
}